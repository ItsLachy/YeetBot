# Copyright (c) 2018 Taizo Simpson
# This file is part of the YeetBot project
#
# YeetBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

""" A module abstracting an SQL database for keeping track of user balances

This module provides tools for wrapping an SQL database and using it to track users,
their ip addresses, their Discord accounts, and their balances.  This module makes
heavy use of SQL Alchemy to accomplish this.

Example:

	>>> bnk = Bank('sqlite:///database.db') # Create a bank wrapping an sqlite database in the local file "database.db"
	>>> bnk.register_user('426121037952974869', '127.0.0.1', 'Tazial') # Register a new user named Tazial with a Discord ID and connecting from localhost
	>>> bnk.change_balance('426121037952974869', 420) # Add 420 credits to Tazial's account
	True
	>>> bnk.change_balance('426121037952974869', -351) # Remove 351 credits from Tazial's account
	True
	>>> bnk.get_balance('426121037952974869') # Query the number of credits in Tazial's account
	69
"""

import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
import importlib

Base = declarative_base()

class User(Base):
	"""A ORM Model for a user registered with the bot

	A user is effectively a link between an ip address and a Discord account, with a
	balance tied to it.  As a result, the Discord ID and IP address must be valid and
	present.  A name is optional, but highly recomended, for understandability.

	Attributes:
		discord_id (str): The unique, numeric, 18 digit ID assigned to this user by
			Discord.  Also serves as the primary key.
		ip_address (str): The user's primary IP address
		name (str): The user's display name, purely cosmetic
		balance (int): The number of credits in this user's name
		payments (list): A list containing one payment for every
			`ContinuousPaymentModule` the user has recieved money from.  A
			One-to-Many relationship with the `Payment` model.
	"""
	__tablename__ = 'users'
	discord_id = sqlalchemy.Column(sqlalchemy.String(18), primary_key=True)
	ip_address = sqlalchemy.Column(sqlalchemy.String(45), unique=True, nullable=False)
	name = sqlalchemy.Column(sqlalchemy.String(32))
	balance = sqlalchemy.Column(sqlalchemy.Integer(), default=0, nullable=False)
	payments = sqlalchemy.orm.relationship("Payment", back_populates='user')

class Payment(Base):
	"""An ORM tracking the total paid to users from a `ContinuousPaymentModel`.

	This class can be treated as internal, but is documented for code quality
	regardless.  This class is used to track the total amount of money the
	users have recieved from a specific continuous payment source.  No more
	than one entry should exist per `User` per `ContinuousPaymentModule` in
	use.  It should be noted that this is the total amount ever given to this
	user from this method.  Even if payments were given on multiple occasions
	and some of it was spent, there should only be one of these, and its value
	should be the sum of the two payments.

	Attributes:
		id (int): The primary key of this payment
		cpm_name (str): The `name` attribute of the `ContinuousPaymentModule`
			this was created for.
		user_id (str): The `discord_id` attribute of the `User` this payment
			was made to.
		total_paid (int): A BigInteger containing the total amount of money
			this user has recieved from the cpm. 
		user (User): A reference to the `User` with user_id (Thanks sqlalchemy!)
	"""
	__tablename__ = 'payments'
	id = sqlalchemy.Column(sqlalchemy.Integer(), primary_key=True)
	cpm_name = sqlalchemy.Column(sqlalchemy.String(30))
	user_id = sqlalchemy.Column(sqlalchemy.String(18), sqlalchemy.ForeignKey('users.discord_id'))
	total_paid = sqlalchemy.Column(sqlalchemy.BigInteger())
	user = sqlalchemy.orm.relationship("User", back_populates='payments')

class ContinuousPaymentModule():
	"""An external source continuously feeding money into a user's account

	This class represents an income source the feeds money into the user's
	account at a continuous rate.  This means that income can build up even
	when the program is not running, and the user's balance is always
	increasing.  An example of when this might be useful is if you have access
	to a user's total playtime statistics for your server, and you want to pay
	them at a fixed rate per time spent playing on the server
	
	In order to create the illusion of a continuous income feed without
	actually constantly updating the SQL database, a CPM should be registered
	with a `Bank`.  Then, whenever the user's balance is queried, the real
	balance of the user is added to the amount of money that they are earned
	from the CPM since the user's real balance was last updated.  When the
	user's real balance is updated, the amount of money earned from the CPM is
	added to it, and the counter on the CPM is reset (i.e. a note is kept about
	the total money earned by the user at the last deposit, and future queries
	about how much the user has earned from the CPM since the last deposit give
	an answer relative to this number.)

	Attributes:
		name (str): A unique name used to track previous payments made from
			this method.  Should be no longer than 30 characters
		engine (sqlalchemy.engine.Engine): The SQL database that should be used
			to track how much each user has earned.  It is recomended that this
			be the same database that is tracking the users.
		get_total (function): A function that accepts a `User` and returns
			the total amount of currency that this user has earned since the
			begining of time.  The difference between this amount and the total
			that the user has actually recieved will be added to the user's
			account.
		config (dict): A dict containing any extra config passed into the
			total_payment_function.
		get_pay_modifier (function[discord_id:str --> Number]): A function that
			returns a multiplier that should be used against any income a user
			with the provided Discord ID earns.  For example, if this returns
			1.5 for a certain Discord ID, that user earns 50% more they
			normally would.  This defaults to always returning 1.

	Args:
		name (str): See the `name` attribute
		engine (sqlalchemy.engine.Engine): See the `engine` attribute
		config (dict): A dict containing any extra config passed into the
			total_payment_function.
		total_payment_function (function): The function used for the
			`get_total` method
		get_pay_modifier (function[discord_id:str --> Number]): See the
			`get_pay_modifier` attribute
	"""
	def __init__(self, name, engine, config, total_payment_function, pay_modifier_function=lambda x: 1):
		assert len(name) <= 30
		self.name = name
		self.engine = engine
		self.config = config
		self._get_total = total_payment_function
		self.get_pay_modifier = pay_modifier_function
		Base.metadata.create_all(engine)
	
	def from_module_name(module_name, module_config, engine, pay_modifier_function=lambda x: 1):
		"""An alternate constructor for creating a CPM using the name of the module

		The module's name should be the name of the file in the modules folder
		minus the .py extension.  Inside of the module, there should be a
		function called get_total_pay, which is used as the
		total_payment_function.  The name of the module is used as the name of
		the CPM
		must still be passed in manually

		Arguments:
			module_name (str): See the above description for more info
			engine (sqlalchemy.engine.Engine): See the `engine` attribute
			get_pay_modifier (function[discord_id:str --> Number]): See the
				`get_pay_modifier` attribute

		Returns:
			ContinuousPaymentModule: The newly constructed CPM
		"""
		module = importlib.import_module('modules.'+module_name)
		return ContinuousPaymentModule(module_name, engine, module_config, module.get_total_pay, pay_modifier_function)

	def get_total(self, user):
		"""The total amount of money a user has earned from this CPM over all time.

		This function simply wraps the function given through the
		`total_payment_function` passed when this instance was created and
		applies the payment modifier from the `get_payment_modifier` attribute.

		Args:
			user (User): The user to find the total for

		Returns:
			Number: The total amount of money this user has earned through this CPM
		"""
		total = self._get_total(user, self.config) * self.get_pay_modifier(user.discord_id)
		return total
	
	def get_change(self, user, commit_int_amount = False):
		"""Get the amount of money a user has accrued since this stat was last reset.

		This is effectively the difference between the amount of money that has
		been given to the user and the amount that they have earned, or in
		other words, the amount currently owed to them.  It is important to
		reset this value any time it is added to the user's balance as it is
		stored in the database.
		
		This value can be used to compute the user's live expendable funds, as
		it is in the Bank.get_balance method.

		Args:
			user (User): The user to check the change in money for
			commit_int_amount (bool): Instead of returning the total difference
				between the amount the user has recieved and the amount they
				have been paid, if the `commit_int_amount` argument is True,
				then this value is rounded down to the nearest integer amount,
				and the amount of money that has been recorded as having been
				paid to the user is increased by that much.  It is assumed that
				the returned value ends up getting added to the user's balance
				so that their net worth (i.e. get_change(user) +
				users_balance) doesn't change.  The purpose of only commiting
				the whole part of the balance is to keep lossy floating-points
				out of the database being used to store the user's balance as
				well as the database storing how much the user has been paid.

		Returns:
			Number: The difference between the amount of money the user has
			been paid from this CPM and the amount they have earned from this
			CPM
		"""
		total_income = self.get_total(user)
		if commit_int_amount:
			total_income = int(total_income)

		session = sqlalchemy.orm.session.Session(self.engine)
		payment = session.query(Payment).filter(
			Payment.user_id == user.discord_id, 
			Payment.cpm_name == self.name
		).one_or_none()
		difference = 0
		if payment:
			difference = total_income - payment.total_paid
			if commit_int_amount:
				payment.total_paid = total_income
		else:
			difference =  total_income
			if commit_int_amount and total_income > 0:
				payment = Payment(cpm_name=self.name, user_id=user.discord_id, total_paid=total_income)
				session.add(payment)
		session.commit()
		session.close()
		return difference

class Bank():
	"""The representation of the database and the collection of users in it

	The bank serves as an interface for the underlieing database through a number of
	higher level methods allowing the query and modification of the balances of the
	users in the database.  Although the database doesn't have to belong exclusively
	to the user table, this class only interacts with that part of the table.

	Adding continuous payment systems allows the bank to automatically and
	continuously add money to the users accounts, as configured in the
	continuous payment module.

	Attributes:
		database (sqlalchemy.engine.Engine): The database engine used by sqlalchemy
			to interact with the database
		Session (function): A function that takes no arguments and returns a session
			for connecting to and modifying the database
		cpms (set[ContinuousPaymentModule]): A set containing all CPMs this
			bank should use to credit users.  Feel free to add CPMs to this set
			in order to add them to the bank
	
	Args:
		database_string (str): The url indicating the type of database and how to
			connect.  Defaults to a local sqlite database in the file
			`bank.db`.  See https://docs.sqlalchemy.org/en/latest/core/engines.html
			for specifics on how this works
		engine (sqlalchemy.engine.Engine): The database engine to be used.
			Mutually exclusive to `database_string`, and used in preference.

	"""
	def __init__(self, engine=None, database_string='sqlite:///bank.db'):
		self.database = engine or sqlalchemy.create_engine(database_string)
		self.Session = sqlalchemy.orm.sessionmaker(bind=self.database)
		Base.metadata.create_all(self.database)
		self.cpms = set()
		self.get_pay_modifier = lambda x: 1
	
	def register_user(self, discord_id, ip_address, name=None):
		"""Register a new user with the database

		This is a method for adding a new user to the database, using a higher-level
		interface than directly using the database itself.  The new user will be
		created with no balance, and the credentials provided as arguments.  If
		`name` is not provided, it will be left null in the database.

		The arguments provided to this method parallel the attributes of the `User`
		class.

		Args:
			discord_id (str): The user's 18 digit Discord ID
			ip_address (str): The user's ip address, either IPv6 or v4
			name (str): A display name for the user, defaulting to None
		Returns:
			bool: True if the addition to the database was successfull, False
			if the user already exists, causing the addition to fail.
		"""
		user = User(discord_id=discord_id, ip_address=ip_address, name=name)
		session = self.Session()
		try:
			session.add(user)
			session.commit()
			return True
		except sqlalchemy.exc.IntegrityError:
			return False
	
	def change_balance(self, discord_id, amt):
		"""Modifies the balance of a pre-existing user

		This is a high level method for increasing or decreasing the balance of a
		user.  Using this method also updates the user's balance with any
		income they have earned from continuous payment modules.

		Args:
			discord_id (str): The Discord ID of the user who's balance is to be
				affected.  Must correspond to the id of a user already in the
				database.
			amt (int): The amount to change the user's balance by.  Can be negative
				to decrease the balance

		Returns:
			bool: True if the user was successfully found and their balance
			incremented, False otherwise
		"""
		session = self.Session()
		user = session.query(User).filter(User.discord_id == discord_id).one_or_none()
		success = False
		if user:
			user.balance =\
				user.balance +\
				sum(cpm.get_change(user, True) for cpm in self.cpms) +\
				amt
			success = True
		session.commit()
		session.close()
		return success
	
	def get_balance(self, discord_id):
		"""Queries the balance of a specific user

		A high-level getter for the balance of a specific user in the database.
		This balance is a "virtual" balance in that it contains any gains the
		user might have earned from the continuous income sources, even if
		these gains don't show up in the SQL database yet.  This money is still
		spendable however.

		Args:
			discord_id (str): The unique ID of this user as assigned by Discord.

		Returns:
			int: The balance of the queried user, or `None` if they weren't found
		"""
		session = self.Session()
		user = session.query(User).filter(User.discord_id == discord_id).one_or_none()
		session.close()
		return int(user.balance + sum(cpm.get_change(user) for cpm in self.cpms)) if user else None
	
	def change_ip(self, discord_id, target_ip):
		"""Update the IP address of the given user to the target ip address

		A high-level method to change out the ip address of a user in the
		database.  In the process of doing this, any pending payments (i.e. the
		user's "virtual" balance) will be commited, and the owed balances
		are reset to zero to prevent the user from losing money due to the
		rapid change in possible reported CPM balanes.

		Args:
			discord_id (str): The unique ID of this user as assigned by Discord.
			target_ip (sr): The ip address to change this user's ip address to

		Raises:
			sqlalchemy.orm.exc.NoResultFound: There is no user with the given id
			sqlalchemy.exc.IntegrityError: The given ip already exists
		"""
		self.change_balance(discord_id, 0) # Commit pending balances
		session = self.Session()
		user = session.query(User).filter(User.discord_id == discord_id).one()
		user.ip_address = target_ip

		# Reset payments
		for cpm in self.cpms:
			cpm.get_change(user, True) # Discard any change in balance since the last commit	
		session.commit()
