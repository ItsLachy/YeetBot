# Copyright (c) 2018 Taizo Simpson
# This file is part of the YeetBot project
#
# YeetBot is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import datetime

async def on_purchase(user, item, bot, config):
	""" Event triggered when an item with this module is purchased

	This is used to set actions to correspond to this purchase.  Anything
	scriptable with python should be accomplishable.  For example, an item could be
	configured to correspond to an in-game item by adding a config variable for
	that item with it's in-game id, and the configuring on_purchase to spawn an
	item in-game for the user with the specified id

	Args:
		user (discord.User): The Discord user who purchased the item
		item (shop.Item): The item that was purchased.  Please treat this as
			read-only unless you really know what you're doing.
		bot (discord.ext.commands.bot.Bot): The Discord bot handling this purchase
		config (list or dict): The relevant section of config from the main config
			file.  For a module called `example`, the `value` property would be
			stored in `config['value']`, and would correspond to the config
			property `modules.example.value`.
	Returns:
		bool: This will always return True, but False can be returned as an
		alternative if the user declines the purchase, or if it otherwise
		fails.
	"""
	user_discord_id = user.id
	user_display_name = user.name
	item_price = item.price
	item_name = item.name
	user_balance = bot.bank.get_balance(user_discord_id)
	await bot.say(f'{user_display_name} purchased {item_name} using {item_price} of their {user_balance} credits')
	await bot.send_message(user, config['message']%item_name)
	return True
	# Item config can also be accessed using item.config[property]

def get_total_pay(user, config):
	"""Total amount of currency that a user deserves to have paid to them
	
	This includes past payments.  For example, if this were using playtime
	logs, the user might get paid 10 credits for every minute in game.  If the
	user has played for a total of one hour all time, this function should
	return `600`.

	This implemenation is very simple for demonstration purposes, and pays the
	user one credit every minute elapsed since midnight on 2018 June 13. 

	Args:
		user (bank.User): The user for which the value is calculated
		config (dict): Any arguments in the `example` config section
	"""
	return (datetime.datetime.now()  - datetime.datetime(2018, 6, 13)).total_seconds()/60
